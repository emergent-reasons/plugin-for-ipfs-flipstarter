##!/usr/bin/env python3
#
# Author: Calin Culianu <calin.culianu@gmail.com>
# Author: Dagur Valberg Johannsson <dagurval@pvv.ntnu.no>
# Copyright (C) 2019 Calin Culianu
# Copyright (C) 2020 Dagur Valberg Johannsson
# LICENSE: MIT
#
import os
import urllib
import json
import requests
import time
from datetime import datetime, date

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from decimal import Decimal as PyDecimal  # Qt 5.12 also exports Decimal

from electroncash.bitcoin import Hash, bfh, bh2u
from electroncash.bitcoin import int_to_hex, base_decode, base_encode
from electroncash.bitcoin import COIN, TYPE_ADDRESS, TYPE_SCRIPT, OpCodes
from electroncash.address import ScriptOutput, Address
from electroncash.i18n import _
from electroncash.plugins import BasePlugin, hook
from electroncash.transaction import InputValueMissing
from electroncash.transaction import Transaction
from electroncash.transaction import OPReturn
from electroncash.util import NotEnoughFunds
from electroncash.util import PrintError
from electroncash.util import UserCancelled
from electroncash.util import print_error, to_bytes, format_satoshis
from electroncash_gui.qt.util import *
from electroncash_gui.qt.amountedit import AmountEdit, BTCAmountEdit, MyLineEdit, BTCSatsByteEdit
from electroncash_gui.qt.util import EnterButton, Buttons, CloseButton
from electroncash_gui.qt.util import OkButton, WindowModalDialog

from .recipients import Recipients

from .funderutil import cancel_pledge
from .funderutil import coin_to_outpoint
from .funderutil import decode_template
from .funderutil import get_pledge_output_summary
from .funderutil import get_pledges
from .funderutil import memorize_pledge
from .funderutil import sats_to_bch
from .funderutil import serialize_pledge_input
from .funderutil import template_to_tx
from .funderutil import sanitize_data
from .walletutil import is_wallet_compatible
from .walletutil import get_wallet_address
from .known_addr import summarize_outputs

class Plugin(BasePlugin):
    def __init__(self, parent, config, name):
        super().__init__(parent, config, name)
        # list of 'Instance' objects
        self.instances = list()
        # pointer to Electrum.gui singleton
        self.gui = None
        self.config = config
        self.is_slp = False

    def shortName(self):
        return _("IPFS Flipstarters")

    def settings(self):
        from .resources import qInitResources  # lazy importing
        icon = ":flipstarter/resources/settings_white.png" if ColorScheme.dark_scheme else ":flipstarter/resources/settings.png"
        return QIcon(icon)

    def icon(self):
        from .resources import qInitResources  # lazy importing
        return QIcon(":flipstarter/resources/icon.png")

    def iconLarge(self):
        from .resources import qInitResources  # lazy importing
        return QIcon(":flipstarter/resources/icon64.png")

    def description(self):
        return _("Raise funds cooperatively")

    def thread_jobs(self):
        return list()

    def on_close(self):
        """
        BasePlugin callback called when the wallet is disabled among other things.
        """
        ct = 0
        for instance in self.instances:
            instance.close()
            ct += 1
        self.instances = list()
        self.print_error("on_close: closed %d extant instances" % (ct) )

    @hook
    def init_qt(self, qt_gui):
        """
        Hook called when a plugin is loaded (or enabled).
        """
        self.gui = qt_gui
        # We get this multiple times.  Only handle it once, if unhandled.
        if len(self.instances):
            return

        # These are per-wallet windows.
        for window in self.gui.windows:
            self.load_wallet(window.wallet, window)

    @hook
    def load_wallet(self, wallet, window):
        """
        Hook called when a wallet is loaded and a window opened for it.
        """
        self.instances.append(Instance(self, wallet, window))

    @hook
    def close_wallet(self, wallet):
        for instance in self.instances:
            if instance.wallet == wallet:
                iname = instance.diagnostic_name()
                instance.close()
                # pop it from the list of instances, python gc will remove it
                self.instances.remove(instance)
                self.print_error("removed instance:", iname)
                return

class Instance(QWidget, PrintError):
    """Encapsulates a wallet-specific instance."""

    sig_network_updated = pyqtSignal()
    sig_user_tabbed_to_us = pyqtSignal()
    sig_user_tabbed_from_us = pyqtSignal()
    sig_window_moved = pyqtSignal()
    sig_window_resized = pyqtSignal()
    sig_window_activation_changed = pyqtSignal()
    sig_window_unblocked = pyqtSignal()
    contribution_scan_thread_update = pyqtSignal(object)

    def __init__(self, plugin, wallet, window):
        super().__init__()
        self.plugin = plugin
        self.wallet = wallet
        self.window = window
        self.fx = plugin.gui.daemon.fx
        self.config = plugin.gui.config
        self.wallet_name = os.path.split(wallet.storage.path)[1]
        
        window.installEventFilter(self)

        self.already_warned_incompatible = False
        self.disabled = False
        self.completions = QStringListModel()
        self.require_fee_update = False
        self.not_enough_funds = False
        self.op_return_toolong = False

        self.decimal_point = self.config.get('decimal_point', 8)
        self.fee_unit = self.config.get('fee_unit', 0)
        self.num_zeros = int(self.config.get('num_zeros',0))

        self.campaign = None
        self.ipfs_gateway = self.config.get('ipfs_gateway', "https://dweb.link/") or "https://dweb.link/"
        self.ipfs_http_api = self.config.get('ipfs_http_api', "http://localhost:5001") or "http://localhost:5001"
        self.init_contribution_scan()

        self.send_grid = grid = QGridLayout()
        grid.setSpacing(8)
        grid.setColumnStretch(3, 1)

        self.make_campaign_header(grid, 0)
        self.make_cid_row(grid, 1)
        self.make_title_row(grid, 2)
        self.make_expires_row(grid, 3)
        self.make_recipients_row(grid, 4)
        
        self.make_header_row(grid, 5, _('\nPledge'))
        self.make_pledge_row(grid, 6)
        self.make_alias_row(grid, 8)
        self.make_comment_row(grid, 9)
        self.make_buttons_row(grid, 10)
        self.make_contributions_row(grid, 11)
        
        vbox_g = QVBoxLayout()
        vbox_g.addLayout(grid)
        vbox_g.addStretch()

        hbox = QHBoxLayout()
        hbox.addLayout(vbox_g)

        vbox = QVBoxLayout(self)
        vbox.addLayout(hbox)
        vbox.addStretch(1)
        vbox.addWidget(self.contributions_list_label)
        vbox.addWidget(self.contribution_list)
        vbox.setStretchFactor(self.contribution_list, 1000)

        window.tabs.addTab(self, plugin.icon(), "IPFS Flipstarters")

    def settings_dialog(self):
        d = WindowModalDialog(self.window.top_level_window(), _("Email settings"))
        d.setMinimumSize(500, 200)

        vbox = QVBoxLayout(d)
        vbox.addWidget(QLabel(_('Configuration and settings')))
        grid = QGridLayout()
        vbox.addLayout(grid)

        grid.addWidget(QLabel(_('\n\nPrimary IPFS:')), 0, 0)
        grid.addWidget(QLabel('Api'), 1, 0)
        ipfs_http_api_e = QLineEdit()
        ipfs_http_api_e.setText(self.ipfs_http_api)
        grid.addWidget(ipfs_http_api_e, 1, 1)

        grid.addWidget(QLabel('Gateway'), 2, 0)
        ipfs_gateway_e = QLineEdit()
        ipfs_gateway_e.setText(self.ipfs_gateway)
        grid.addWidget(ipfs_gateway_e, 2, 1)

        grid.addWidget(QLabel(_('\nOptional secondary IPFS:')), 3, 0)
        grid.addWidget(QLabel('Api'), 4, 0)
        secondary_ipfs_http_api_e = QLineEdit()
        secondary_ipfs_http_api_e.setText("")
        grid.addWidget(secondary_ipfs_http_api_e, 4, 1)

        grid.addWidget(QLabel('Gateway'), 5, 0)
        secondary_ipfs_gateway_e = QLineEdit()
        secondary_ipfs_gateway_e.setText("")
        grid.addWidget(secondary_ipfs_gateway_e, 5, 1)

        vbox.addStretch()
        vbox.addLayout(Buttons(CloseButton(d), OkButton(d)))

        if not d.exec_():
            return

        self.ipfs_http_api = str(ipfs_http_api_e.text())
        if self.ipfs_http_api:
            self.config.set_key('ipfs_http_api', self.ipfs_http_api)

        self.ipfs_gateway = str(ipfs_gateway_e.text())
        if self.ipfs_gateway:
            self.config.set_key('ipfs_gateway', self.ipfs_gateway)

    def make_header_row(self, grid, row, title):
        label = QLabel(title)
        font = QFont()
        font.setPointSize(16)
        label.setFont(font)
        grid.addWidget(label, row, 0)
        
    def make_campaign_header(self, grid, row):
        self.make_header_row(grid, row, _('Campaign'))
        self.menu_button = QToolButton()
        font = QFont()
        font.setPointSize(16)
        self.menu_button.setFont(font)
        self.menu_button.setPopupMode(QToolButton.InstantPopup)
        self.menu_button.setArrowType(Qt.NoArrow)
        self.menu_button.setObjectName("menu")

        self.menu = QMenu()
        action = QAction()
        action.setIcon(self.plugin.settings())
        self.menu.addAction(_("Refresh"), self.update_campaign_data)
        self.menu.addAction(_("Configure"), self.settings_dialog)
        self.menu_button.setMenu(self.menu)
        self.menu_button.setDefaultAction(action)
        grid.addWidget(self.menu_button, 0, 1, 1, 3, Qt.AlignRight)

    def make_cid_row(self, grid, row):
        self.campaign_cid_e = ButtonsLineEdit()
        msg = _('IPFS CID or URL of the campaign. Optionally, you may upload a campaign.json file describing the campaign.')
        label = HelpLabel(_('&CID or URL'), msg)
        label.setBuddy(self.campaign_cid_e)
        qmark = ":icons/question-mark-dark.svg" if ColorScheme.dark_scheme else ":icons/question-mark-light.svg"
        qmark_help_but = HelpButton(msg, button_text='', fixed_size=False, icon=QIcon(qmark), custom_parent=self)
        self.campaign_cid_e.textChanged.connect(self.update_campaign_data)
        self.campaign_cid_e.addWidget(qmark_help_but, index=0)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.campaign_cid_e, row, 1, 1, -1)
    
    def make_title_row(self, grid, row):
        self.campaign_title_e = QLabel(_('Title'))
        label = QLabel(_('Title'))
        label.setBuddy(self.campaign_title_e)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.campaign_title_e, row, 1, 1, -1)
        self.campaign_title_e.setText("")
    
    def make_expires_row(self, grid, row):
        self.campaign_expires_e = QLabel(_('Expires'))
        label = QLabel(_('Expires'))
        label.setBuddy(self.campaign_expires_e)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.campaign_expires_e, row, 1, 1, -1)
        self.campaign_expires_e.setText("")
    
    def make_recipients_row(self, grid, row):
        self.payto_e = Recipients(self.window)
        label = QLabel(_('Recipients'))
        label.setBuddy(self.payto_e)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.payto_e, row, 1, 1, -1)
        self.payto_e.setText("")

        completer = QCompleter(self.payto_e)
        completer.setCaseSensitivity(False)
        self.payto_e.setCompleter(completer)
        completer.setModel(self.completions)
    
    def make_alias_row(self, grid, row):    
        self.pledge_alias_e = QLineEdit()
        label = QLabel(_('Alias'))
        label.setBuddy(self.pledge_alias_e)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.pledge_alias_e, row, 1, 1, -1)

    def make_comment_row(self, grid, row):
        self.pledge_comment_e = QLineEdit()
        label = QLabel(_('Comment'))
        label.setBuddy(self.pledge_comment_e)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.pledge_comment_e, row, 1, 1, -1)

    def format_amount(self, x, is_diff=False, whitespaces=False):
        return format_satoshis(x, self.num_zeros, self.decimal_point, is_diff=is_diff, whitespaces=whitespaces)

    def make_pledge_row(self, grid, row):
        self.amount_e = BTCAmountEdit(self.get_decimal_point)
        msg = _('Amount to be sent.') + '\n\n' \
              + _('The amount will be displayed in red if you do not have enough funds in your wallet.') + ' ' \
              + _('Note that if you have frozen some of your addresses, the available funds will be lower than your total balance.') + '\n\n' \
              + _('Keyboard shortcut: type "!" to send all your coins.')
        label = HelpLabel(_('Pledge &amount'), msg)
        label.setBuddy(self.amount_e)
        grid.addWidget(label, row, 0)
        grid.addWidget(self.amount_e, row, 1)

        self.fiat_send_e = AmountEdit(self.fx.get_currency if self.fx else '')
        if not self.fx or not self.fx.is_enabled():
            self.fiat_send_e.setVisible(False)
        grid.addWidget(self.fiat_send_e, row, 2, Qt.AlignLeft)
        self.amount_e.frozen.connect(
            lambda: self.fiat_send_e.setFrozen(self.amount_e.isReadOnly()))

        self.max_button = EnterButton(_("&Max"), self.spend_max)
        self.max_button.setFixedWidth(100)
        self.max_button.setCheckable(True)
        grid.addWidget(self.max_button, row, 3, Qt.AlignLeft)
        
        msg = _('Bitcoin Cash transactions are in general not free. A transaction fee is paid by the sender of the funds.') + '\n\n'\
              + _('The amount of fee can be decided freely by the sender. However, transactions with low fees take more time to be processed.') + '\n\n'\
              + _('A suggested fee is automatically added to this field. You may override it. The suggested fee increases with the size of the transaction.')
        self.fee_e_label = HelpLabel(_('Fee'), msg)

        self.fee_e = BTCAmountEdit(self.get_decimal_point)
        self.fee_e.setReadOnly(True)
        # if not self.config.get('show_fee', False):
        #     self.fee_e.setVisible(False)
        self.fee_e.textEdited.connect(self.update_fee)
        # This is so that when the user blanks the fee and moves on,
        # we go back to auto-calculate mode and put a fee back.
        self.fee_e.editingFinished.connect(self.update_fee)
        self.connect_fields(self, self.amount_e, self.fiat_send_e, self.fee_e)

        grid.addWidget(self.fee_e_label, row + 1, 0)
        grid.addWidget(self.fee_e, row + 1, 1)
        
        def reset_max(text):
            self.max_button.setChecked(False)
            self.max_button.setEnabled(True)
    
        self.amount_e.textEdited.connect(reset_max)
        self.fiat_send_e.textEdited.connect(reset_max)

        def entry_changed():
            text = ""
            if self.not_enough_funds:
                amt_color, fee_color = ColorScheme.RED, ColorScheme.RED
                text = _( "Not enough funds" )
                c, u, x = self.wallet.get_frozen_balance()
                if c+u+x:
                    text += ' (' + self.format_amount(c+u+x).strip() + ' ' + self.base_unit() + ' ' +_("are frozen") + ')'

            elif self.fee_e.isModified():
                amt_color, fee_color = ColorScheme.DEFAULT, ColorScheme.DEFAULT
            elif self.amount_e.isModified():
                amt_color, fee_color = ColorScheme.DEFAULT, ColorScheme.BLUE
            else:
                amt_color, fee_color = ColorScheme.BLUE, ColorScheme.BLUE
            opret_color = ColorScheme.DEFAULT
            if self.op_return_toolong:
                opret_color = ColorScheme.RED
                text = _("OP_RETURN message too large, needs to be no longer than 220 bytes") + (", " if text else "") + text

            # self.statusBar().showMessage(text)
            self.amount_e.setStyleSheet(amt_color.as_stylesheet())
            self.fee_e.setStyleSheet(fee_color.as_stylesheet())
        
        self.amount_e.textChanged.connect(entry_changed)
        self.fee_e.textChanged.connect(entry_changed)

    def make_buttons_row(self, grid, row):
        self.save_pledge_button = QPushButton(_('&Pledge'))
        self.save_pledge_button.clicked.connect(self.on_confirm_pledge)

        self.new_pledge_button = QPushButton(_('&Clear'))
        self.new_pledge_button.clicked.connect(self.on_clear_pledge)

        buttons = QHBoxLayout()
        buttons.addWidget(self.save_pledge_button)
        buttons.addWidget(self.new_pledge_button)
        buttons.addStretch(1)
        grid.addLayout(buttons, row, 1, 1, -1)

    def make_contributions_row(self, grid, row):
        self.contributions_list_label = QLabel(_('Contributions'))
        from .contribution_list import ContributionList
        self.contribution_list = ContributionList(self.window)
        self.contribution_list.setVisible(True)
        self.contributions_list_label.setBuddy(self.contribution_list)

    def connect_fields(self, window, btc_e, fiat_e, fee_e):

        def edit_changed(edit):
            if edit.follows:
                return
            edit.setStyleSheet(ColorScheme.DEFAULT.as_stylesheet())
            fiat_e.is_last_edited = (edit == fiat_e)
            amount = edit.get_amount()
            rate = self.fx.exchange_rate() if self.fx else None
            if rate is None or amount is None:
                if edit is fiat_e:
                    btc_e.setText("")
                    if fee_e:
                        fee_e.setText("")
                else:
                    fiat_e.setText("")
            else:
                if edit is fiat_e:
                    btc_e.follows = True
                    btc_e.setAmount(int(amount / PyDecimal(rate) * COIN))
                    btc_e.setStyleSheet(ColorScheme.BLUE.as_stylesheet())
                    btc_e.follows = False
                    if fee_e:
                        window.update_fee()
                else:
                    fiat_e.follows = True
                    fiat_e.setText(self.fx.ccy_amount_str(
                        amount * PyDecimal(rate) / COIN, False))
                    fiat_e.setStyleSheet(ColorScheme.BLUE.as_stylesheet())
                    fiat_e.follows = False

        btc_e.follows = False
        fiat_e.follows = False
        fiat_e.textChanged.connect(partial(edit_changed, fiat_e))
        btc_e.textChanged.connect(partial(edit_changed, btc_e))
        btc_e.editingFinished.connect(self.do_update_fee)
        fiat_e.is_last_edited = False

    def fetch_ipfs_path(self, url):
        # TODO God willing: add a button to fetch rather than automatically
        # TODO God willing: don't play with /ipns/ return values
        try:
            response = requests.head(url, allow_redirects=True, timeout=5)
            return str(response.headers["X-IPFS-PATH"])
        except:
            return None

    def update_campaign_data(self):
        cidOrUrl = self.campaign_cid_e.text()
        self.on_clear_pledge(True)

        ipfsPath = self.fetch_ipfs_path(cidOrUrl)

        # Try as CID (no py-cid available to verify before hand and too many possible multiformats)
        if not ipfsPath:
            ipfsPath = self.fetch_ipfs_path(self.ipfs_gateway + "/ipfs/" + str(cidOrUrl))

        # Fail if no CID
        if not ipfsPath:
            self.campaign_cid_e.setStyleSheet(ColorScheme.RED.as_stylesheet())
            return
        else:
            self.campaign_cid_e.setStyleSheet(ColorScheme.BLUE.as_stylesheet())

        campaign_json = None
        if ipfsPath:
            if not ipfsPath.endswith('campaign.json'):
                ipfsPath = ipfsPath + ("" if ipfsPath.endswith("/") else "/") + "campaign.json"

            try:
                response = requests.get(self.ipfs_gateway + ipfsPath, allow_redirects=True, timeout=5)
                campaign_json = response.json()
            except requests.exceptions.ReadTimeout as e:
                self.campaign_cid_e.setStyleSheet(ColorScheme.YELLOW.as_stylesheet())
                self.show_error("Failed to fetch campaign data. Try again.")
            except requests.exceptions.JSONDecodeError as e:
                self.campaign_cid_e.setStyleSheet(ColorScheme.RED.as_stylesheet())

        if campaign_json:
            try:
                # TODO God willing: keep buttons disabled if expiration passed or hasn't started
                expires = campaign_json["expires"]
                title = str(campaign_json["title"])
                def parseRecipients(r):
                    return r["address"] + ", " + str(r["satoshis"])

                self.campaign = campaign_json
                recipients = map(parseRecipients, campaign_json["recipients"])
                self.payto_e.setText("\r\n".join(recipients))
                self.campaign_title_e.setText(title)
                self.campaign_expires_e.setText(datetime.utcfromtimestamp(expires).strftime('%m-%d-%Y'))
                # Notify scanner 
                self.contribution_list.clear()
                self.contribution_scan_thread_last_recipients = None
                self.start_contribution_scan()
            except Exception as e:
                self.show_error(e)
                self.campaign_cid_e.setStyleSheet(ColorScheme.RED.as_stylesheet())

    def is_expired(self):
        if self.campaign:
            expires = datetime.utcfromtimestamp(self.campaign["expires"])
            today = datetime.combine(date.today(), datetime.min.time())
            return today > expires
        else:
            return False
        
    def init_contribution_scan(self):
        self.contribution_scan_thread = None
        self.contribution_scan_thread_ping = threading.Event()
        self.contribution_scan_thread_stopping = False
        self.contribution_scan_thread_last_recipients = None

        self.contribution_scan_thread_update.connect(self.contributions_update)

    def start_contribution_scan(self):
        if self.contribution_scan_thread is None:
            self.contribution_scan_thread = threading.Thread(name='Flipstarter-scan_contributions', target=self.scan_contributions_loop)
            self.contribution_scan_thread_stopping = False
            self.contribution_scan_thread.start()
        else:
            self.contribution_scan_thread_ping.set()

    def stop_contribution_scan(self):
        self.contribution_scan_thread_stopping = True
        # Notify thread to check stopping variable
        self.contribution_scan_thread_ping.set()
        self.contribution_scan_thread = None
    
    def scan_contributions_loop(self):
        while not self.contribution_scan_thread_stopping:
            current_recipients = self.payto_e.toPlainText()
            address = self.payto_e.get_recipient_address()
            recipient_scripthash = address.to_scripthash_hex() if address else None

            if address and not self.contribution_scan_thread_last_recipients == current_recipients:
                self.contribution_scan_thread_last_recipients = current_recipients
                self.get_contributions(current_recipients, recipient_scripthash)

            # notifications = self.get_notifications()
            self.contribution_scan_thread_ping.wait(10)
            self.contribution_scan_thread_ping.clear()

    def contributions_update(self, data):
        try:
            (notification, commitment, commitment_input, commitment_data, status) = data
            average_byte_per_contribution = 296

            notification_height = str(notification["height"]) if notification else ""
            commitment_tx_hash = str(commitment["tx_hash"]) if commitment else ""
            commitment_data_hash = str(commitment["cid"]) if commitment else ""
            commitment_value = str(commitment_input["value"] - average_byte_per_contribution) if commitment_input else ""
            alias = commitment_data["alias"] if commitment_data else ""
            comment = commitment_data["comment"] if commitment_data else ""

            treeRow = QTreeWidgetItem([
                notification_height,
                alias,
                comment,
                commitment_value,
                commitment_tx_hash, 
                commitment_data_hash,
                status
            ])

            if status == "VALID":
                treeRow.setForeground(6, QBrush(ColorScheme.GREEN.as_color(background=True)))
            else:
                treeRow.setForeground(6, QBrush(ColorScheme.RED.as_color(background=True)))

            self.contribution_list.addTopLevelItem(treeRow)
        except Exception as e:
            print(e)

    def get_contributions(self, id, script_hash):
        try:
            notification_txs = self.wallet.network.synchronous_get(('blockchain.scripthash.get_history', [script_hash]), timeout=5)
            
            # print(notification_txs)

            for notification_tx_info in notification_txs:
                try:
                    status = "VALID"
                    
                    # TODO God willing: validate structure here
                    commitment = self.get_notification_commitment(notification_tx_info["tx_hash"])

                    # Check if no commitment data or can't parse (common, ex. normal payment)
                    if not commitment:
                        continue
                    
                    commitment_input = self.get_input_from_commitment(commitment)

                    # Only fails if commitment data doesn't have real transaction on chain or has an invalid structure
                    if status == "VALID" and not commitment_input:
                        status = "NOT FOUND"

                    if status == "VALID" and not self.is_commitment_input_valid(commitment_input):
                        status = "INVALID SIG"
                    
                    if status == "VALID" and not self.is_unspent(commitment_input):
                        status = "REVOKED"

                    commitment_data = self.get_pledge_data(commitment["cid"]) if status == "VALID" and "cid" in commitment else None
                    
                    self.contribution_scan_thread_update.emit((notification_tx_info, commitment, commitment_input, commitment_data, status))

                except Exception as e:
                    print("Flipstarter error: " + str(e))
                    continue

        except Exception as e:
            # TODO God willing: mark in contribution table that something went wrong, iA.
            print("Flipstarter error: " + str(e))
            return

    def get_pledge_data(self, data_hash):
        defaultRet = { "alias": "Anonymous", "comment": "" }

        try:
            if not data_hash:
                return defaultRet

            ipfsUrl = self.ipfs_gateway + "/ipfs/" + str(data_hash)
            response = requests.get(ipfsUrl, allow_redirects=True, timeout=1)
            campaign_json = response.json()
            # TODO God willing: sanitize
            alias = campaign_json["alias"]
            comment = campaign_json["comment"]
            return {
                "alias": alias or defaultRet["alias"],
                "comment": comment or defaultRet["comment"]
            }
        except Exception as e:
            print("Flipstarter error:get_pledge_data: " + str(e))
            # TODO God willing: reveal error to user in menu action
            return defaultRet

    def get_tx(self, tx_hash):
        try:
            tx_hex = self.wallet.network.synchronous_get(('blockchain.transaction.get', [tx_hash]), timeout=5)
            tx = Transaction(tx_hex)
            tx.deserialize()
            return tx
        except Exception as e:
            print("Flipstarter error:get_tx: " + str(e))
            return None

    def get_notification_commitment(self, notification_tx_hash):
        try:
            notification_tx = self.get_tx(notification_tx_hash)
            for output in notification_tx.outputs():
                _type = output[0]
                _address = output[1]
                if not TYPE_SCRIPT == _type or not _address.is_opreturn():
                    continue
                
                # Assuming OP_RETURN OP_PUSHDATA1 <length> <...data>
                commitment_data_bytes = _address.script[3:]
                offset = 0
                def read_slice(n):
                    nonlocal offset
                    offset += n
                    return commitment_data_bytes[offset - n:offset]
                
                def read_uint32():
                    nonlocal offset
                    offset += 4
                    return int.from_bytes(commitment_data_bytes[offset - 4:offset], 'little')

                tx_hash = read_slice(32).hex()
                tx_index = read_uint32()
                seq_num = read_uint32()
                cid_bytes = read_slice(34)
                cid = base_encode(cid_bytes, base=58) if cid_bytes != bytes(34) else None
                unlocking_script = commitment_data_bytes[offset:].hex()

                commitment = { 
                    "tx_hash": tx_hash, 
                    "tx_index": tx_index, 
                    "seq_num": seq_num, 
                    "cid": cid, 
                    "unlocking_script": unlocking_script,
                }

                return commitment
        except Exception as e:
            print("Flipstarter::parse_notification_for_commitment: " + str(e))
            pass

    def get_input_from_commitment(self, commitment):
        try:
            # TODO God willing: fetch the commitment txHash and txIndex
            pledge_tx_sig = commitment['unlocking_script']
            pledged_tx_hash = commitment['tx_hash']
            pledged_tx_index = commitment['tx_index']
            pledged_tx = self.get_tx(pledged_tx_hash)
            
            (_type, pledger_address, pledged_value) = pledged_tx.outputs()[pledged_tx_index]

            return {
                'address': pledger_address,
                'type': 'p2pkh',
                'prevout_hash': pledged_tx_hash,
                'prevout_n': pledged_tx_index,
                'sequence': 0xffffffff,
                'value': pledged_value,
                'scriptSig': pledge_tx_sig,
            }
        except Exception as e:
            print("Flipstarter error:get_input_from_commitment: " + str(e))
            return None

    def is_commitment_input_valid(self, commitment_input, reason=[]):
        #  validate the signature is good, iA.
        pledge_tx = HackedTransaction.from_io(inputs=[commitment_input], outputs=self.payto_e.outputs)
        pledge_tx.version = 2
        pledge_tx_preimage = Hash(bfh(pledge_tx.serialize_preimage(0, nHashType=0x000000c1)))
        pledge_tx_sig_bytes = bytes.fromhex(commitment_input["scriptSig"])

        pledger_signature = pledge_tx_sig_bytes[1:-35]
        pledger_pubkey = pledge_tx_sig_bytes[-33:]
        
        return pledge_tx.verify_signature(pledger_pubkey, pledger_signature, pledge_tx_preimage, reason=reason)

    def is_unspent(self, commitment_input):       
        try:
            pledger_script_hash = commitment_input["address"].to_scripthash_hex()
            pledger_utxos = self.wallet.network.synchronous_get(('blockchain.scripthash.listunspent', [pledger_script_hash]), timeout=5)
            for utxo in pledger_utxos:
                if commitment_input["prevout_hash"] == utxo['tx_hash'] and commitment_input["prevout_n"] == utxo['tx_pos']:
                    return True
            else:
                return False
        except:
            return False

    def get_decimal_point(self):
        return self.decimal_point

    def spend_max(self):
        self.max_button.setChecked(True)
        self.do_update_fee()

    def update_fee(self):
        self.require_fee_update = True

    def get_custom_fee_text(self, fee_rate = None):
        if not self.config.has_custom_fee_rate():
            return ""
        else:
            if fee_rate is None: fee_rate = self.config.custom_fee_rate() / 1000.0
            return str(round(fee_rate*100)/100) + " sats/B"

    def do_update_fee(self):
        campaign_total = self.payto_e.total_amount

        if not campaign_total:
            return

        '''Recalculate the fee.  If the fee was manually input, retain it, but
        still build the TX to see if there are enough funds.
        '''
        freeze_fee = (self.fee_e.isModified()
                      and (self.fee_e.text() or self.fee_e.hasFocus()))
        fee_rate = None

        user_amount = self.amount_e.get_amount() 
        amount = '!' if self.max_button.isChecked() else user_amount if not user_amount or user_amount <= campaign_total else campaign_total
        
        if amount is None:
            if not freeze_fee:
                self.fee_e.setAmount(None)
            self.not_enough_funds = False
        else:
            try:
                result = self.get_pledge_total(amount)
                self.amount_e.setAmount(str(result["amount"]))
                self.fee_e.setAmount(result["fee"])
                self.not_enough_funds = False
                self.op_return_toolong = False
            except NotEnoughFunds:
                self.not_enough_funds = True
                if not freeze_fee:
                    self.fee_e.setAmount(None)
                return
            except BaseException:
                self.show_error(str(sys.exc_info()[1]))
                return

    def get_notification_setup_value(self):
        # Enough to send a notification transaction (p2pkh) w/ DUST value and 220 byte OP_RETURN output
        dust = 546
        estimateFee = 449

        return estimateFee + dust

    def get_pledge_total(self, amount):         
        average_byte_per_contribution = 296
        frozen_pledge_addr = get_wallet_address(self.wallet)
        setup_notification_value = self.get_notification_setup_value()

        outputs = [
            # Reserve enough for estimated pledge result (so add fullfillment fee per input)
            (TYPE_ADDRESS, frozen_pledge_addr, amount if amount == "!" else amount + average_byte_per_contribution),
            # Warn if not enough to satisfy estimated notification costs
            (TYPE_ADDRESS, frozen_pledge_addr, setup_notification_value),
        ]

        coins = self.wallet.get_utxos(exclude_frozen=True, exclude_slp=True)

        tx = self.wallet.make_unsigned_transaction(coins, outputs, self.config, None)
        fee = tx.get_fee() + setup_notification_value
        # Recalculate amount since "!" isn't known until afterwards
        realAmount = tx.output_value() - average_byte_per_contribution if amount == "!" else amount
        
        result = {
            "tx": tx,
            "fee": fee,
            "amount": realAmount
        }

        campaign_total = self.payto_e.total_amount

        if amount == "!" and result["amount"] > campaign_total:
            return self.get_pledge_total(campaign_total)
        
        if not amount == "!" and result["amount"] > campaign_total:
            result["amount"] = campaign_total

        return result

    def get_wallet_password(self):
        password = None
        if self.wallet.has_password():
            password = self.window.password_dialog(
                    _('Your wallet is encrypted.') + '\n' +
                    _('Please enter your password to continue.'))
            if not password:
                raise Exception(_("Password required"))
        return password

    def on_commitment_selection_change(self):
        text = "" # self.ui.commitment.toPlainText()
        if len(text) == 0:
            # Nothing to select
            return

        # cursor = self.ui.commitment.textCursor()

        if cursor.isNull():
            # Not sure why cursor is sometimes null, but without
            # this check, this signal will recurse forever.
            # See issue #25
            return

        if len(text) == len(cursor.selectedText()):
            # We have full selection.
            return

        # Partial selection, force full selection
        # self.ui.commitment.selectAll()

    def on_clear_pledge(self, keepCid=False):
        self.max_button.setDisabled(False)
        self.max_button.setChecked(False)
        self.amount_e.setText('')
        self.fee_e.setText('')
        self.pledge_alias_e.setText('')
        self.pledge_comment_e.setText('')
        self.fiat_send_e.setText('')
        if not keepCid:
            self.payto_e.setText('')
            self.campaign = None
            self.campaign_title_e.setText('')
            self.campaign_expires_e.setText('')
            self.campaign_cid_e.setText('')
            self.contribution_list.clear()
        
    def on_confirm_pledge(self):
        donation_sats = self.amount_e.get_amount()

        if not donation_sats:
            return

        real_donation_sats = self.amount_e.get_amount() + 296
            
        donation_bch_human = sats_to_bch(donation_sats)
        total_bch_human = sats_to_bch(self.payto_e.total_amount)

        expiresDt = datetime.utcfromtimestamp(self.campaign["expires"])
        todayDt = datetime.combine(date.today(), datetime.min.time())
        daysAgo = todayDt - expiresDt
        if self.is_expired() and not self.window.question(_("This campaign expired {} days ago on {}. Are you sure you want to make a pledge?").format(daysAgo.days, expiresDt.strftime('%m-%d-%Y'))):
            return self.show_error(_("Pledge cancelled by user"))

        pledge_question = "{} {} BCH towards {} BCH for: \n\n{}".format(
            _("Do you want to pledge"),
            donation_bch_human,
            total_bch_human,
            "\n".join(self.payto_e.get_recipient_addresses())
        )

        if not self.window.question(pledge_question):
            return self.show_error(_("Pledge cancelled by user"))

        if not self.wallet.network:
            return self.show_error(_("Electron Cash is not connected to the network"))

        try:
            password = self.get_wallet_password()
            tx, pledge_input = self.create_tx_to_self(real_donation_sats, password)
        except NotEnoughFunds as _e:
            return self.show_error(_("Not enough funds in wallet"))
        except Exception as e:
            e_str = str(e)
            if len(e_str) == 0:
                e_str = "Unknown"
            return self.show_error("{}: {}".format(_("Failed to create a transaction to ourselves"), e_str))

        try:
            pledge_tx = Transaction.from_io(inputs=[pledge_input], outputs=self.payto_e.outputs)
            pledge_tx.version = 2
            signed = self.sign_pledge_tx(pledge_tx, password)
            txin = signed.inputs()[0]
            scriptsig = signed.input_script(txin, False, tx._sign_schnorr)
        except Exception as e:
            return self.show_error("{}: {}".format(_("Failed to sign pledge"), e))

        if self.wallet.network:
            ok, details = self.wallet.network.broadcast_transaction(tx)
            if not ok:
                return self.show_error("{}: {}".format(_("Failed to broadcast transaction"), details))

            self.wallet.set_label(
                details,
                _("Flipstarter pledge to {}. To cancel, see FAQ at flipstarter.cash.".format(
                    self.campaign["title"])))
        else:
            # Should not happen, as we already checked for this above
            # before freezing coins
            return self.show_error(_("Connection error"))

        frozen = self.wallet.set_frozen_coin_state([pledge_input], True)

        if frozen != 1:
            return self.show_error(_("Failed to freeze pledge"))

        time.sleep(2)
        
        commitment_hash = bytes(34).hex()

        try:
            alias = self.pledge_alias_e.text()
            comment = self.pledge_comment_e.text()

            if alias or comment:
                # TODO God willing: add a button to fetch rather than automatically
                commitment_data = {
                    "recipients": self.campaign["recipients"],
                    "txHash": pledge_input["prevout_hash"],
                    "txIndex": pledge_input["prevout_n"],
                    "unlockingScript": scriptsig,
                    "seqNum": pledge_input["sequence"],
                    # TODO God willing: sign the alias/comment to the pledge data. avoid non-pledgers sending in pledges.
                    # might add scripthash as well, iA.
                    "alias": alias or "",
                    "comment": comment or ""
                }

                files = {'file': ("test", json.dumps(commitment_data), 'application/json', {}) }
                
                response = requests.post(self.ipfs_http_api + "/api/v0/add?cid-version=0", files=files, timeout=5)
                response_json = response.json()

                # TODO God willing: try again, use a different server, don't toss frozen coin, iA.
                if not "Hash" in response_json:
                    raise Exception("Invalid hash returned from IPFS Api")
                
                # TODO God willing: rather than opening and verifying, just fetch from a different area (configured gateway)
                #   then deserialize and json decode it to check it matches what we expect
                verify_commitment_question = _("Is this commitment hash suitable? \n\n") + self.ipfs_gateway + "/ipfs/" + response_json["Hash"]
                if not self.window.question(verify_commitment_question):
                    raise Exception("commitment hash rejected by user")

                commitment_hash = base_decode(response_json["Hash"], None, base=58).hex()
        except Exception as e:
            self.show_error("{}: {}".format(_("Failed to upload comment/alias to IPFS"), e))
            
            anonymous_commitment_question = _("Would you like to continue without uploading an alias/comment to IPFS?")
            if not self.window.question(anonymous_commitment_question):
                frozen = self.wallet.set_frozen_coin_state([pledge_input], False)
                return self.show_error(_("Pledge cancelled by user"))
            else:
                commitment_hash = bytes(34).hex()
            
        try:
            notification_message = pledge_input["prevout_hash"] + int_to_hex(pledge_input['prevout_n'], 4) + int_to_hex(pledge_input['sequence'], 4) + commitment_hash + scriptsig
            serialized_message_length = int_to_hex(len(bytes.fromhex(notification_message)))
            op_return_script = ScriptOutput.protocol_factory(bytes.fromhex(
                int_to_hex(OpCodes.OP_RETURN) +
                int_to_hex(OpCodes.OP_PUSHDATA1) +
                serialized_message_length +
                notification_message))

            notification_outputs = [
                (TYPE_ADDRESS, self.payto_e.get_recipient_address(), 546),
                (TYPE_SCRIPT, op_return_script, 0)
            ]

            # Either remove coins (and add change) from unbroadcasted consolidation tx or wait until broadcasted and hope utxos refresh by this operation
            notification_tx = self.wallet.mktx(notification_outputs, password, self.plugin.config)

            ok, details = self.wallet.network.broadcast_transaction(notification_tx)
            if not ok:
                return self.show_error("{}: {}".format(_("Failed to broadcast transaction"), details))
        except Exception as e:
            frozen = self.wallet.set_frozen_coin_state([pledge_input], False)

            # TODO God willing: possibly rollback commitment? (Easy way to try again with same params, iA?)
            return self.show_error("{}: {}".format(_("Failed to make notification transaction"), e))

        self.window.show_message(_("Your pledge is prepared."))
        self.contribution_list.clear()
        self.contribution_scan_thread_last_recipients = None
        self.contribution_scan_thread_ping.set()
        self.on_clear_pledge(True)        

    def create_tx_to_self(self, amount, password):
        coins = self.wallet.get_utxos(exclude_frozen=True,
                                      exclude_slp=True)

        frozen_pledge_addr = get_wallet_address(self.wallet)


        tx = self.wallet.make_unsigned_transaction(
                inputs=coins,
                outputs=[(TYPE_ADDRESS, frozen_pledge_addr, amount)],
                config=self.plugin.config)

        self.wallet.sign_transaction(tx, password)

        # find our output pledge output
        pledge_input = None
        for i, (t, addr, v) in enumerate(tx.outputs()):
            if t != TYPE_ADDRESS:
                continue
            if addr != frozen_pledge_addr:
                continue
            if v != amount:
                continue

            # match
            pledge_input = {
                'address': addr,
                'type': 'p2pkh',
                'prevout_hash': tx.txid(),
                'prevout_n': i,
                'sequence': 0xffffffff,
                'value': amount
            }
            self.wallet.add_input_sig_info(pledge_input, addr)
            break

        if pledge_input is None or pledge_input['address'] is None:
            raise Exception(_("Pledge output missing"))

        return tx, pledge_input

    def sign_pledge_tx(self, tx, password):
        # SIGHASH_ANYONECANPAY | SIGHASH_ALL is not supported by
        # electrum, hence this ugly hack.
        signed = False
        tx.__class__ = HackedTransaction
        if self.wallet.is_watching_only():
            raise Exception(_("Watch only wallet"))
        for k in self.wallet.get_keystores():
            try:
                if k.can_sign(tx):
                    k.sign_transaction(tx, password)
                    signed = True
                else:
                    self.print_error("Key store {} cannot sign {}".format(k, tx))
            except UserCancelled:
                continue

        if not signed:
            raise Exception(_("Unable to sign"))

        return tx

    def show_error(self, msg):
        self.window.show_error(msg=msg, title=_("Error: ") + self.plugin.shortName(), parent=self.window)

    def on_cancel_pledges(self):
        """Cancel all known pledge coins"""
        actual_coins = self.wallet.get_utxos(exclude_frozen=False,
                                             exclude_slp=True)
        existing_pledges = get_pledges(self.wallet.storage, actual_coins)

        # notify and stop if no pledges to cancel
        if not existing_pledges:
            self.window.show_message(_("No known pledges to cancel - if there are any, please unfreeze and spend them manually in 'Coins' tab"))
            return

        # confirm before canceling
        if not self.window.question(
                "{} ({})".format(_("Do you want to cancel all current pledges?"),
                                 len(existing_pledges))):
            return

        # do the cancellations
        success_count = 0
        failed_coins = list()
        try:
            password = self.get_wallet_password()
        except Exception as e:
            self.window.show_message(_("Failed to cancel pledges: {}". format(e)))
            return

        for c in existing_pledges:
            try:
                cancel_pledge(self.wallet, password, self.plugin.config, c)
                success_count += 1
            except Exception as e:
                self.print_error("Failed to cancel pledge: {}".format(repr(e)))
                failed_coins.append(c)

        # notify the user how everything went
        msg = "{}: {}".format(_("Cancelled pledges"), success_count)
        if failed_coins:
            msg += "\n{}\n{}".format(_("Failed to cancel some pledges. Please unfreeze and spend them in 'Coins' tab"),
                                     "\n".join(coin_to_outpoint(c) for c in failed_coins))
        self.window.show_message(msg)

    def on_network(self, event, *args):
        # network thread
        if event == 'updated' and (not args or args[0] is self.wallet):
            self.sig_network_updated.emit()  # this passes the call to the gui thread
        elif event == 'verified':  # grr.. old api sucks
            self.sig_network_updated.emit()  # this passes the call to the gui thread
        elif event in ('wallet_updated', 'verified2') and args[0] is self.wallet:
            self.sig_network_updated.emit()  # this passes the call to the gui thread
        elif event == 'blockchain_updated':
            self.sig_network_updated.emit()  # this passes the call to the gui thread

    def on_user_tabbed_to_us(self):
        warn_user = False
        try:
            if self.incompatible and not self.already_warned_incompatible:
                self.already_warned_incompatible = True
                warn_user = True
        except AttributeError:
            # Exception happens because I don't think all
            # wallets have the is_watching_only method
            pass

        if warn_user:
            if self.is_slp:
                msg = _("This is an incompatible wallet type.") + "\n\n" \
                    + _("SLP token wallets are not supported, as a safety feature.")
            else:
                msg = _("This is an incompatible wallet type.") + "\n\n" \
                    + _("The plugin only supports imported private key or standard spending wallets.")
            self.window.show_error(msg=msg,
                                   title="{} - {}".format(self.plugin.shortName(), _("Incompatible Wallet")),
                                   parent=self.window)

    def disable_if_incompatible(self):
        if not self.incompatible:
            return

        self.disabled = True
        warning = "{} {}".format(
            _("The plugin is disabled. This wallet is incompatible:"),
            self.incompatible_error)

        self.print_error(warning)
        self.set_status(warning, is_warning = True)

        gbs = [
            self.ui.confirm,
            self.ui.menu,
            self.ui.pledger_alias,
            self.ui.pledger_comment,
            # self.ui.template_in,
        ]
        for gb in gbs:
            gb.setEnabled(False)  # disable all controls

    def event(self, event):
        """overrides QObject: a hack used to detect when the prefs
           screen was closed or when our tab comes to foreground."""
        if event.type() in (QEvent.WindowUnblocked, QEvent.ShowToParent) and self.wallet:
            if event.type() == QEvent.ShowToParent:
                self.sig_user_tabbed_to_us.emit()
            else:
                self.sig_window_unblocked.emit()

            # if window unblocked, maybe user changed prefs.
            # inform our managers to refresh() as maybe base_unit changed, etc.
            self.refresh_all()

        elif event.type() in (QEvent.HideToParent,):
            # user left our UI. Tell interested code about
            # this (mainly the PopupLabel cares)
            self.sig_user_tabbed_from_us.emit()

        # Make real QWidget implementation actually handle the event
        return super().event(event)

    def eventFilter(self, window, event):
        """Spies on events to parent window to figure out
           when the user moved or resized the window, and announces
           that fact via signals."""
        if window == self.window:
            if event.type() == QEvent.Move:
                self.sig_window_moved.emit()
            elif event.type() == QEvent.Resize:
                self.sig_window_resized.emit()
            elif event.type() == QEvent.ActivationChange:
                self.sig_window_activation_changed.emit()
        return super().eventFilter(window, event)

    def refresh_all(self):
        self.print_error("refresh_all")

    def wallet_has_password(self):
        try:
            return self.wallet.has_password()
        except AttributeError:  # happens on watching-only wallets which don't have the requisite methods
            return False

    # Uncomment to test object lifetime and make sure Qt is deleting us.
    #def __del__(self):
    #    print("**** __del__ ****")

    # called by self.plugin on wallet close - deallocate all resources and die.
    def close(self):
        self.stop_contribution_scan()
        self.print_error("Close called on an Instance")
        if self.window:
            self.window.removeEventFilter(self)
            ix = self.window.tabs.indexOf(self)
            if ix > -1:
                self.window.tabs.removeTab(ix)
                # since qt doesn't delete us, we need to explicitly
                # delete ourselves, otherwise the QWidget lives around
                # forever in memory
                self.deleteLater()
        self.disabled = True

        # trigger object cleanup sooner rather than later!
        self.window, self.plugin, self.wallet_name = (None,) * 3

    # overrides PrintError super
    def diagnostic_name(self):
        if hasattr(self.wallet, "diagnostic_name") and callable(self.wallet.diagnostic_name):
            return self.wallet.diagnostic_name() + ".flipstarter"
        return super().diagnostic_name() + ".flipstarter"

    def set_status(self, text, is_warning = False):
        return ""

#####

# Hacks to sign with SIGHASH_ANYONECANPAY from jcramer
# https://github.com/simpleledger/Electron-Cash-SLP
class HackedTransaction(Transaction):
    def serialize_preimage(self, i, nHashType=0x00000041, **kwargs):
        use_cache = kwargs.get("use_cache")
        from electroncash.bitcoin import int_to_hex, var_int, bh2u
        if not (nHashType & 0xff) in [0xc1]:
            raise ValueError("wrong hashtype for this hack")

        nVersion = int_to_hex(self.version, 4)
        nHashType = int_to_hex(nHashType, 4)
        nLocktime = int_to_hex(self.locktime, 4)

        txin = self.inputs()[i]
        outpoint = self.serialize_outpoint(txin)
        preimage_script = self.get_preimage_script(txin)
        scriptCode = var_int(len(preimage_script) // 2) + preimage_script
        try:
            amount = int_to_hex(txin['value'], 8)
        except KeyError:
            raise InputValueMissing
        nSequence = int_to_hex(txin['sequence'], 4)

        if use_cache is not None:
            hashPrevouts, hashSequence, hashOutputs = self.calc_common_sighash(use_cache=use_cache)
        else:
            hashPrevouts, hashSequence, hashOutputs = self.calc_common_sighash()

        hashPrevouts = "0000000000000000000000000000000000000000000000000000000000000000"
        hashSequence = "0000000000000000000000000000000000000000000000000000000000000000"

        preimage = nVersion + hashPrevouts + hashSequence + outpoint + scriptCode + amount + nSequence + bh2u(hashOutputs) + nLocktime + nHashType
        return preimage

    def sign(self, keypairs, **kwargs):
        for i, txin in enumerate(self.inputs()):
            pubkeys, x_pubkeys = self.get_sorted_pubkeys(txin)
            for j, (pubkey, x_pubkey) in enumerate(zip(pubkeys, x_pubkeys)):
                if self.is_txin_complete(txin):
                    # txin is complete
                    break
                if pubkey in keypairs:
                    _pubkey = pubkey
                    kname = 'pubkey'
                elif x_pubkey in keypairs:
                    _pubkey = x_pubkey
                    kname = 'x_pubkey'
                else:
                    continue
                print_error(f"adding signature for input#{i} sig#{j}; {kname}: {_pubkey} schnorr: {self._sign_schnorr}")
                sec, compressed = keypairs.get(_pubkey)
                self._sign_txin(i, j, sec, compressed)
        print_error("is_complete", self.is_complete())
        self.raw = self.serialize()

    def _sign_txin(self, i, j, sec, compressed, **kwargs):
        from electroncash.bitcoin import public_key_from_private_key, Hash, bfh, bh2u
        '''Note: precondition is self._inputs is valid (ie: tx is already deserialized)'''
        pubkey = public_key_from_private_key(sec, compressed)
        # add signature
        nHashType = 0x000000c1
        pre_hash = Hash(bfh(self.serialize_preimage(i, nHashType)))
        if self._sign_schnorr:
            sig = self._schnorr_sign(pubkey, sec, pre_hash)
        else:
            sig = self._ecdsa_sign(sec, pre_hash)
        reason = []
        if not self.verify_signature(bfh(pubkey), sig, pre_hash, reason=reason):
            print_error(f"Signature verification failed for input#{i} sig#{j}, reason: {str(reason)}")
            return None
        txin = self._inputs[i]
        txin['signatures'][j] = bh2u(sig + bytes((nHashType & 0xff,)))
        txin['pubkeys'][j] = pubkey # needed for fd keys
        return txin
